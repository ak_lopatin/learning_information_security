program PowerMod;

function PowerMod(base, exponent, modulus: Integer): Integer;
var
  result, i: Integer;
begin
  result := 1;
  for i := 1 to exponent do
  begin
    result := (result * base) mod modulus;
  end;
  PowerMod := result;
end;

var
  base, exponent, modulus: Integer;
begin
  write('Enter the base: ');
  readln(base);
  write('Enter the exponent: ');
  readln(exponent);
  write('Enter the modulus: ');
  readln(modulus);
  
  writeln(base, ' raised to the power of ', exponent, ' modulo ', modulus, ' is ', PowerMod(base, exponent, modulus));
end.