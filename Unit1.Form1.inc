procedure InitializeComponent;
    begin
        var resources: System.ComponentModel.ComponentResourceManager := new System.ComponentModel.ComponentResourceManager(typeof(Form1));
        self.tabPage4 := new System.Windows.Forms.TabPage();
        self.tabPage3 := new System.Windows.Forms.TabPage();
        self.textBox4 := new System.Windows.Forms.TextBox();
        self.textBox3 := new System.Windows.Forms.TextBox();
        self.button4 := new System.Windows.Forms.Button();
        self.label8 := new System.Windows.Forms.Label();
        self.tabPage2 := new System.Windows.Forms.TabPage();
        self.label7 := new System.Windows.Forms.Label();
        self.label6 := new System.Windows.Forms.Label();
        self.label5 := new System.Windows.Forms.Label();
        self.textBox2 := new System.Windows.Forms.TextBox();
        self.textBox1 := new System.Windows.Forms.TextBox();
        self.button3 := new System.Windows.Forms.Button();
        self.tabPage1 := new System.Windows.Forms.TabPage();
        self.label4 := new System.Windows.Forms.Label();
        self.label3 := new System.Windows.Forms.Label();
        self.label2 := new System.Windows.Forms.Label();
        self.label1 := new System.Windows.Forms.Label();
        self.textBoxsdvg := new System.Windows.Forms.TextBox();
        self.textBoxOutput := new System.Windows.Forms.TextBox();
        self.textBoxInput := new System.Windows.Forms.TextBox();
        self.button2 := new System.Windows.Forms.Button();
        self.button1 := new System.Windows.Forms.Button();
        self.tabControl1 := new System.Windows.Forms.TabControl();
        self.tabPage3.SuspendLayout();
        self.tabPage2.SuspendLayout();
        self.tabPage1.SuspendLayout();
        self.tabControl1.SuspendLayout();
        self.SuspendLayout();
        // 
        // tabPage4
        // 
        self.tabPage4.Location := new System.Drawing.Point(4, 22);
        self.tabPage4.Name := 'tabPage4';
        self.tabPage4.Padding := new System.Windows.Forms.Padding(3);
        self.tabPage4.Size := new System.Drawing.Size(1006, 551);
        self.tabPage4.TabIndex := 3;
        self.tabPage4.Text := 'tabPage4';
        self.tabPage4.UseVisualStyleBackColor := true;
        // 
        // tabPage3
        // 
        self.tabPage3.Controls.Add(self.textBox4);
        self.tabPage3.Controls.Add(self.textBox3);
        self.tabPage3.Controls.Add(self.button4);
        self.tabPage3.Controls.Add(self.label8);
        self.tabPage3.Location := new System.Drawing.Point(4, 22);
        self.tabPage3.Name := 'tabPage3';
        self.tabPage3.Padding := new System.Windows.Forms.Padding(3);
        self.tabPage3.Size := new System.Drawing.Size(1006, 551);
        self.tabPage3.TabIndex := 2;
        self.tabPage3.Text := 'tabPage3';
        self.tabPage3.UseVisualStyleBackColor := true;
        // 
        // textBox4
        // 
        self.textBox4.Location := new System.Drawing.Point(33, 29);
        self.textBox4.Multiline := true;
        self.textBox4.Name := 'textBox4';
        self.textBox4.Size := new System.Drawing.Size(162, 32);
        self.textBox4.TabIndex := 3;
        self.textBox4.TextChanged += textBox4_TextChanged;
        // 
        // textBox3
        // 
        self.textBox3.Location := new System.Drawing.Point(5, 361);
        self.textBox3.Multiline := true;
        self.textBox3.Name := 'textBox3';
        self.textBox3.ScrollBars := System.Windows.Forms.ScrollBars.Both;
        self.textBox3.Size := new System.Drawing.Size(1000, 189);
        self.textBox3.TabIndex := 0;
        self.textBox3.Visible := false;
        self.textBox3.TextChanged += textBox3_TextChanged;
        // 
        // button4
        // 
        self.button4.Location := new System.Drawing.Point(33, 67);
        self.button4.Name := 'button4';
        self.button4.Size := new System.Drawing.Size(162, 25);
        self.button4.TabIndex := 2;
        self.button4.Text := 'Сгенирировать ';
        self.button4.UseVisualStyleBackColor := true;
        self.button4.Click += button4_Click;
        // 
        // label8
        // 
        self.label8.Font := new System.Drawing.Font('Microsoft Sans Serif', 12, (System.Drawing.FontStyle((System.Drawing.FontStyle.Bold or System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, (System.Byte(204)));
        self.label8.ForeColor := System.Drawing.SystemColors.MenuText;
        self.label8.Location := new System.Drawing.Point(5, 6);
        self.label8.Name := 'label8';
        self.label8.Size := new System.Drawing.Size(275, 20);
        self.label8.TabIndex := 1;
        self.label8.Text := 'Метод серединных квадратов';
        self.label8.Click += label8_Click;
        // 
        // tabPage2
        // 
        self.tabPage2.Controls.Add(self.label7);
        self.tabPage2.Controls.Add(self.label6);
        self.tabPage2.Controls.Add(self.label5);
        self.tabPage2.Controls.Add(self.textBox2);
        self.tabPage2.Controls.Add(self.textBox1);
        self.tabPage2.Controls.Add(self.button3);
        self.tabPage2.Location := new System.Drawing.Point(4, 22);
        self.tabPage2.Name := 'tabPage2';
        self.tabPage2.Padding := new System.Windows.Forms.Padding(3);
        self.tabPage2.Size := new System.Drawing.Size(1006, 551);
        self.tabPage2.TabIndex := 1;
        self.tabPage2.Text := 'Упражнение 2';
        self.tabPage2.UseVisualStyleBackColor := true;
        // 
        // label7
        // 
        self.label7.Location := new System.Drawing.Point(368, 88);
        self.label7.Name := 'label7';
        self.label7.Size := new System.Drawing.Size(267, 23);
        self.label7.TabIndex := 5;
        self.label7.Text := 'Все варианты после расшифровки :';
        self.label7.Click += label7_Click;
        // 
        // label6
        // 
        self.label6.Location := new System.Drawing.Point(27, 88);
        self.label6.Name := 'label6';
        self.label6.Size := new System.Drawing.Size(205, 23);
        self.label6.TabIndex := 4;
        self.label6.Text := 'Зашифрованное сообщение :';
        self.label6.Click += label6_Click;
        // 
        // label5
        // 
        self.label5.Font := new System.Drawing.Font('Microsoft Sans Serif', 24, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (System.Byte(204)));
        self.label5.Location := new System.Drawing.Point(268, 21);
        self.label5.Name := 'label5';
        self.label5.Size := new System.Drawing.Size(471, 53);
        self.label5.TabIndex := 3;
        self.label5.Text := 'Расшифровка шифра Цезаря';
        // 
        // textBox2
        // 
        self.textBox2.Location := new System.Drawing.Point(368, 114);
        self.textBox2.Multiline := true;
        self.textBox2.Name := 'textBox2';
        self.textBox2.Size := new System.Drawing.Size(257, 398);
        self.textBox2.TabIndex := 2;
        // 
        // textBox1
        // 
        self.textBox1.Location := new System.Drawing.Point(6, 114);
        self.textBox1.Name := 'textBox1';
        self.textBox1.Size := new System.Drawing.Size(334, 20);
        self.textBox1.TabIndex := 0;
        self.textBox1.Text := 'уцюьюыгэтьп';
        self.textBox1.TextChanged += textBox1_TextChanged;
        // 
        // button3
        // 
        self.button3.Location := new System.Drawing.Point(27, 140);
        self.button3.Name := 'button3';
        self.button3.Size := new System.Drawing.Size(107, 38);
        self.button3.TabIndex := 1;
        self.button3.Text := 'Расшифровать:';
        self.button3.UseVisualStyleBackColor := true;
        self.button3.Click += button3_Click;
        // 
        // tabPage1
        // 
        self.tabPage1.Controls.Add(self.label4);
        self.tabPage1.Controls.Add(self.label3);
        self.tabPage1.Controls.Add(self.label2);
        self.tabPage1.Controls.Add(self.label1);
        self.tabPage1.Controls.Add(self.textBoxsdvg);
        self.tabPage1.Controls.Add(self.textBoxOutput);
        self.tabPage1.Controls.Add(self.textBoxInput);
        self.tabPage1.Controls.Add(self.button2);
        self.tabPage1.Controls.Add(self.button1);
        self.tabPage1.Location := new System.Drawing.Point(4, 22);
        self.tabPage1.Name := 'tabPage1';
        self.tabPage1.Padding := new System.Windows.Forms.Padding(3);
        self.tabPage1.Size := new System.Drawing.Size(1006, 551);
        self.tabPage1.TabIndex := 0;
        self.tabPage1.Text := 'Упражнение 1';
        self.tabPage1.UseVisualStyleBackColor := true;
        // 
        // label4
        // 
        self.label4.Location := new System.Drawing.Point(27, 156);
        self.label4.Name := 'label4';
        self.label4.Size := new System.Drawing.Size(254, 23);
        self.label4.TabIndex := 8;
        self.label4.Text := 'Закодированный (раскодированный) текст :';
        // 
        // label3
        // 
        self.label3.Location := new System.Drawing.Point(391, 59);
        self.label3.Name := 'label3';
        self.label3.Size := new System.Drawing.Size(237, 23);
        self.label3.TabIndex := 7;
        self.label3.Text := 'Введите сдвиг шифра';
        // 
        // label2
        // 
        self.label2.Location := new System.Drawing.Point(27, 59);
        self.label2.Name := 'label2';
        self.label2.Size := new System.Drawing.Size(358, 23);
        self.label2.TabIndex := 6;
        self.label2.Text := 'Введите текст, который нужно закодировать (раскадировать):';
        // 
        // label1
        // 
        self.label1.Font := new System.Drawing.Font('Microsoft Sans Serif', 20, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (System.Byte(204)));
        self.label1.Location := new System.Drawing.Point(214, 3);
        self.label1.Name := 'label1';
        self.label1.Size := new System.Drawing.Size(579, 34);
        self.label1.TabIndex := 5;
        self.label1.Text := 'Кодирование с помощью шифра Цезаря';
        self.label1.Click += label1_Click;
        // 
        // textBoxsdvg
        // 
        self.textBoxsdvg.Location := new System.Drawing.Point(391, 85);
        self.textBoxsdvg.Name := 'textBoxsdvg';
        self.textBoxsdvg.Size := new System.Drawing.Size(237, 20);
        self.textBoxsdvg.TabIndex := 4;
        // 
        // textBoxOutput
        // 
        self.textBoxOutput.Location := new System.Drawing.Point(27, 182);
        self.textBoxOutput.Name := 'textBoxOutput';
        self.textBoxOutput.Size := new System.Drawing.Size(334, 20);
        self.textBoxOutput.TabIndex := 1;
        // 
        // textBoxInput
        // 
        self.textBoxInput.Location := new System.Drawing.Point(27, 85);
        self.textBoxInput.Name := 'textBoxInput';
        self.textBoxInput.Size := new System.Drawing.Size(334, 20);
        self.textBoxInput.TabIndex := 0;
        self.textBoxInput.TextChanged += textBox1_TextChanged;
        // 
        // button2
        // 
        self.button2.Location := new System.Drawing.Point(189, 111);
        self.button2.Name := 'button2';
        self.button2.Size := new System.Drawing.Size(156, 30);
        self.button2.TabIndex := 3;
        self.button2.Text := 'Расшифровать';
        self.button2.UseVisualStyleBackColor := true;
        self.button2.Click += button2_Click;
        // 
        // button1
        // 
        self.button1.Location := new System.Drawing.Point(27, 111);
        self.button1.Name := 'button1';
        self.button1.Size := new System.Drawing.Size(156, 30);
        self.button1.TabIndex := 2;
        self.button1.Text := 'Зашифровать';
        self.button1.UseVisualStyleBackColor := true;
        self.button1.Click += button1_Click;
        // 
        // tabControl1
        // 
        self.tabControl1.Controls.Add(self.tabPage1);
        self.tabControl1.Controls.Add(self.tabPage2);
        self.tabControl1.Controls.Add(self.tabPage3);
        self.tabControl1.Controls.Add(self.tabPage4);
        self.tabControl1.Location := new System.Drawing.Point(12, 12);
        self.tabControl1.Name := 'tabControl1';
        self.tabControl1.SelectedIndex := 0;
        self.tabControl1.Size := new System.Drawing.Size(1014, 577);
        self.tabControl1.TabIndex := 0;
        // 
        // Form1
        // 
        self.ClientSize := new System.Drawing.Size(1026, 601);
        self.Controls.Add(self.tabControl1);
        self.Icon := (System.Drawing.Icon(resources.GetObject('$this.Icon')));
        self.Name := 'Form1';
        self.Text := 'Проект по ИБ';
        self.Load += Form1_Load;
        self.tabPage3.ResumeLayout(false);
        self.tabPage3.PerformLayout();
        self.tabPage2.ResumeLayout(false);
        self.tabPage2.PerformLayout();
        self.tabPage1.ResumeLayout(false);
        self.tabPage1.PerformLayout();
        self.tabControl1.ResumeLayout(false);
        self.ResumeLayout(false);
    end;
