unit strokavfile;

interface
procedure WriteStringToFile(afileName: string; atext: string);
implementation
procedure WriteStringToFile(afileName: string; atext: string);
var
  fileVar: text;
begin

  Assign(fileVar, afileName); 
  Rewrite(fileVar);
  Writeln(fileVar, atext); 
  Close(fileVar); 
end;


end.
