﻿unit RSAsafer;

interface

type
  TPublicKey = record
    n, e: Integer;
  end;

procedure EncryptSymbolRSA(symbol: integer; publicKey: TPublicKey; var encryptedSymbol: Integer);
function PowerMod(base, exponent, modulus: Integer): Integer;
implementation

procedure EncryptSymbolRSA(symbol: integer; publicKey: TPublicKey; var encryptedSymbol: Integer);
begin
  encryptedSymbol := PowerMod(symbol, publicKey.e, publicKey.n); // Шифруем символ с помощью открытого ключа
end;

function PowerMod(base, exponent, modulus: Integer): Integer;

begin
  result := 1;
  while exponent > 0 do
  begin
    if odd(exponent) then
      result := (result * base) mod modulus;
    base := (base * base) mod modulus;
    exponent := exponent div 2;
  end;
end;
end.

