﻿unit simvolvkod;

interface
procedure simvolvkod1(str:string; var s:string);
implementation
procedure simvolvkod1(str: string; var s: string);
var 
  s1, s2: string;
  i: integer;
begin 
  s := '';
  
  for i := 1 to Length(str) do 
  begin
    s1 := IntToStr(Ord(str[i]));
    s2 := Copy('00000000', 1, 8 - Length(s1)) + s1;
    s := s + s2 + ' ';
  end;
end;

end.

