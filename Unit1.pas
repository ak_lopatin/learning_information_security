﻿unit Unit1;

interface

uses System, System.Drawing, System.Windows.Forms;

type
  Form1 = class(Form)
    procedure button1_Click(sender: Object; e: EventArgs);
    procedure textBox1_TextChanged(sender: Object; e: EventArgs);
    procedure button2_Click(sender: Object; e: EventArgs);
    procedure tabPage1_TextChanged(sender: Object; e: EventArgs);
    procedure label1_Click(sender: Object; e: EventArgs);
    procedure button3_Click(sender: Object; e: EventArgs);
    procedure label6_Click(sender: Object; e: EventArgs);
    procedure label7_Click(sender: Object; e: EventArgs);
    procedure Form1_Load(sender: Object; e: EventArgs);
    procedure label8_Click(sender: Object; e: EventArgs);
    procedure button4_Click(sender: Object; e: EventArgs);
    procedure textBox4_TextChanged(sender: Object; e: EventArgs);
    procedure textBox3_TextChanged(sender: Object; e: EventArgs);
    procedure button5_Click(sender: Object; e: EventArgs);
    procedure butt_simvkod_Click(sender: Object; e: EventArgs);
  {$region FormDesigner}
  internal
    {$resource Unit1.Form1.resources}
    tabPage4: TabPage;
    tabPage3: TabPage;
    textBox4: TextBox;
    textBox3: TextBox;
    button4: Button;
    label8: &Label;
    tabPage2: TabPage;
    label7: &Label;
    label6: &Label;
    label5: &Label;
    textBox2: TextBox;
    textBox1: TextBox;
    button3: Button;
    tabPage1: TabPage;
    label4: &Label;
    label3: &Label;
    label2: &Label;
    label1: &Label;
    textBoxsdvg: TextBox;
    textBoxOutput: TextBox;
    textBoxInput: TextBox;
    button2: Button;
    button1: Button;
    tabControl1: TabControl;
    {$include Unit1.Form1.inc}
  {$endregion FormDesigner}
  public 
    constructor;
    begin
      InitializeComponent;
    end;
  end;

implementation 
uses RSAsafer,simvolvkod;

procedure Form1.button1_Click(sender: Object; e: EventArgs);

const
  alphabet = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя';

var
  text, encodedText: string;
  i, index: integer;
  sdvg: integer;

begin
  text := textBoxInput.Text;
  encodedText := '';
  
  sdvg := strtoint(TextBoxsdvg.Text);
  
  for i := 1 to Length(text) do
  begin
    index := (Pos(text[i], alphabet) + sdvg) mod Length(alphabet);
    if index = 0 then
      index := Length(alphabet);
    encodedText := encodedText + alphabet[index];
  end;
  textBoxOutput.Text := encodedText;
  label4.Text := 'Раскодированный текст';
end;

procedure Form1.textBox1_TextChanged(sender: Object; e: EventArgs);
begin
end;

procedure Form1.button2_Click(sender: Object; e: EventArgs);
const
  alphabet = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя';

var
  text, encodedText: string;
  i, index: integer;
  sdvg: integer;

begin
  text := textBoxOutput.Text;
  encodedText := '';
  
  sdvg := strtoint(TextBoxsdvg.Text);
  
  for i := 1 to Length(text) do
  begin
    index := (Pos(text[i], alphabet) - sdvg) mod Length(alphabet);
    if index = 0 then
      index := Length(alphabet);
    encodedText := encodedText + alphabet[index];
  end;
  textBoxInput.Text := encodedText;
  label4.Text := 'Закодированный текст';
end;

procedure Form1.tabPage1_TextChanged(sender: Object; e: EventArgs);
begin
  
end;

procedure Form1.label1_Click(sender: Object; e: EventArgs);
begin
  
end;

procedure Form1.button3_Click(sender: Object; e: EventArgs);
const
  alphabet = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя';
var
  text, encodedText: string;
  i, index: integer;
  sdvg: integer;
begin
  text := textBox1.Text;
  encodedText := '';
  
  for sdvg := 1 to 33 do
  begin
    for i := 1 to Length(text) do
    begin
      index := (Pos(text[i], alphabet) - sdvg + Length(alphabet)) mod Length(alphabet);
      if index = 0 then
        index := Length(alphabet);
      encodedText := encodedText + alphabet[index];
    end;
    
    textBox2.Text := textBox2.Text + sdvg + ') ' + encodedText + #13#10;
    encodedText := '';
  end;
end;

procedure Form1.label6_Click(sender: Object; e: EventArgs);
begin
  
end;

procedure Form1.label7_Click(sender: Object; e: EventArgs);
begin
  
end;

procedure Form1.Form1_Load(sender: Object; e: EventArgs);
begin
   for var i:=32 to 1080 do
  begin
    write(i:4,chr(i):2,'  ');
    if i mod 8 = 7 then writeln;
  end; 
end;

procedure Form1.label8_Click(sender: Object; e: EventArgs);
begin
  
end;

procedure Form1.button4_Click(sender: Object; e: EventArgs);
 var
  seed, number, square: integer;
begin
  textBox3.Visible :=true;
  textBox3.Text:='';
  write('Enter seed value (4 digits): ');
  seed:=strtoint(TextBox4.Text);
 
  
  // Генерируем 10 случайных чисел
  for var i := 1 to 10 do
  begin
    // Вычисляем квадрат текущего семени
    square := seed * seed;
    
    // Получаем центральные 4 цифры квадрата
    square := square div 100;
    square := square mod 10000;
    
    // Новое значение семени
    seed := square;
    
    
    // Выводим полученное число
    textBox3.Text:=textBox3.Text + 'Случайное число '+ i +': ' + seed +  #13#10;
  end;
end;

procedure Form1.textBox4_TextChanged(sender: Object; e: EventArgs);
begin
  
end;

procedure Form1.textBox3_TextChanged(sender: Object; e: EventArgs);
begin

end;

procedure Form1.button5_Click(sender: Object; e: EventArgs);
  var
  publicKey: TPublicKey;
  symbolToEncrypt: Integer;
  encryptedSymbol: Integer;

begin
  publicKey.n := 33; // Примерное значение для n
  publicKey.e := 7; // Примерное значение для e

  writeln('Введите символ для шифрования:');
  readln(symbolToEncrypt);

  EncryptSymbolRSA(symbolToEncrypt, publicKey, encryptedSymbol);

  writeln('Шифрованный символ: ', encryptedSymbol);

end;

procedure Form1.butt_simvkod_Click(sender: Object; e: EventArgs);
var a:string;
begin
  a:='';
  simvolvkod1(textBox5.Text,a);
end;
  

end.